import '../styles/index.css'
import store from './store.js'
import {addCritter, useDeck, addPlayer, addGod} from './actions/game_actions.js'
import render from './renderer.js'
import {generateDeck, shuffle} from './deck.js'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import Game from './components/Game.js'

const reactContainer = document.createElement("div")
document.body.appendChild(reactContainer)
ReactDOM.render(
  <Provider store={store}>
    <Game />
  </Provider>,
  reactContainer
)

function animate() {
  requestAnimationFrame( animate )
  render(store.getState().get('game'))
}
animate()

let deck = generateDeck()
shuffle(deck)

store.dispatch(addCritter({x: 1, y: 1}))
store.dispatch(addCritter({x: 2, y: 1}))
store.dispatch(addCritter({x: 1, y: 2}))
store.dispatch(addCritter({x: 2, y: 2}))
store.dispatch(useDeck({deck}))

let gods = ["Blood", "Licht", "Rabbit", "Octopus"]
let god1Index = Math.floor(Math.random() * gods.length)
let god1 = gods.splice(god1Index, 1)[0]
let god2Index = Math.floor(Math.random() * gods.length)
let god2 = gods.splice(god2Index, 1)[0]

store.dispatch(addGod({god: god1}))
store.dispatch(addGod({god: god2}))
store.dispatch(addPlayer({name: "Pogge"}))
store.dispatch(addPlayer({name: "Väskis"}))
store.dispatch(addPlayer({name: "Kroko"}))
store.dispatch(addPlayer({name: "Linda"}))
