import {critterHovered, critterClicked, tileClicked} from './actions/game_actions.js'
import store from './store.js'
import {throttle} from './util.js'
var THREE = require('three')
var raycaster = new THREE.Raycaster()
var mouse = new THREE.Vector2()

export default function init({domElement, scene, camera, width, height}) {
    domElement.onmousemove = throttle(event => {
        onMouseMove({
            x: event.clientX,
            y: event.clientY,
            camera,
            scene,
            width,
            height
        })
    }, 75)

    domElement.onclick = throttle(event => {
        onMouseClick({
            x: event.clientX,
            y: event.clientY,
            camera,
            scene,
            width,
            height
        })
    }, 75)
}

function onMouseMove({x, y, camera, scene, width, height}) {
    mouse.x = ( x / width ) * 2 - 1;
    mouse.y = - ( y / height ) * 2 + 1;
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( scene.children );
    for (let i = 0; i < intersects.length; i++) {
        if (intersects[i].object.critterId) {
            setTimeout(() => {
                store.dispatch(critterHovered({id: intersects[i].object.critterId}))
            })
            return
        }
    }
    store.dispatch(critterHovered({id: null}))
}

function onMouseClick({x, y, camera, scene, width, height}) {
    mouse.x = ( x / width ) * 2 - 1;
    mouse.y = - ( y / height ) * 2 + 1;
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( scene.children );
    for (let i = 0; i < intersects.length; i++) {
        if (intersects[i].object.critterId) {
            setTimeout(() => {
                store.dispatch(critterClicked({id: intersects[i].object.critterId}))
            })
            return
        } else if (intersects[i].object.isFloor) {
            store.dispatch(tileClicked({
                target: {
                    x: intersects[i].object.x,
                    y: intersects[i].object.y
                }
            }))
            return
        }
    }
    store.dispatch(critterClicked({id: null}))
}
