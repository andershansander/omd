import { createStore } from 'redux'
import reducer from './reducers/root_reducer.js'
var store = createStore(reducer)
export default store