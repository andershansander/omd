export const types = {
    "Thunderstorm": "Thunderstorm",
    "Cliff": "Strange looking cliff",

}

const deckDefinition = [
    {
        name: types.Thunderstorm,
        quantity: 4,
    }, {
        name: types.Cliff,
        quantity: 4,
    },
]

export function shuffle(deck) {
    for (let i = deck.length; i; i--) {
        let j = Math.floor(Math.random() * i)
        ;[deck[i - 1], deck[j]] = [deck[j], deck[i - 1]]
    }
}

let idCounter = 1
export function generateDeck() {
    var deck = []
    deckDefinition.forEach(cardType => {
        for (let i = 0; i < cardType.quantity; i++) {
            deck.push({
                name: cardType.name,
                id: idCounter++
            })
        }
    })
    return deck
}