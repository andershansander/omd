var THREE = require('three')

function critter(critterId) {
    var geometry = new THREE.ConeGeometry( 0.1, 1, 32 )
    var material = new THREE.MeshBasicMaterial( {color: 0x223355} )
    var mesh = new THREE.Mesh( geometry, material )
    mesh.rotation.x = 90
    mesh.critterId = critterId
    return mesh
}

function floorSquare({color, x, y}) {
    var geometry = new THREE.PlaneGeometry( 1, 1, 32 )
    var material = new THREE.MeshBasicMaterial( {color: color, side: THREE.DoubleSide} )
    var floor = new THREE.Mesh( geometry, material )
    floor.isFloor = true
    floor.x = x
    floor.y = y
    return floor
}

export default {
    critter,
    floorSquare
}