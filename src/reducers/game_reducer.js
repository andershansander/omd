import {Map, List, fromJS} from 'immutable'
import validTurn from './game_logic/validate_turn.js'
import endTurn from './game_logic/end_turn.js'
import applyCard from './game_logic/card_effects.js'

var idCounter = 1
export default function (state, action) {
    switch (action.type) {
    case 'END_TURN':
        if (!validTurn(state)) {
            console.warn("Cannot end turn since the turn is not valid")
            return state
        }
        state = endTurn(state)
        break
    case 'DRAW_CARD':
        state = drawCard(state, action)
        break
    case 'PLAY_CARD':
        state = playCard(state, action)
        break    
    case 'ADD_GOD':
        state = addGod(state, action)
        break
    case 'ADD_PLAYER':
        state = addPlayer(state, action)
        break
    case 'USE_DECK':
        state = state.set('deck', fromJS(action.deck))
        break
    case 'CREATE_CRITTER':
        state = createCritter(state, action)
        break
    case 'TILE_CLICKED':
        state = tileClicked(state, action)
        break
    case 'CRITTER_HOVERED':
        state = state.set('highlightedCritter', action.id)
        break
    case 'CRITTER_CLICKED':
        state = state.set('selectedCritter', action.id)
        break
    default:
        console.warn('Unhandled game action', action)
    }

    state = state.set('canEndTurn', validTurn(state))
    return state
}

function getCurrentPlayerCards(state) {
    return state.getIn(['players', state.get('currentPlayer'), "cards"])
}

function setCurrentPlayerCards(state, cards) {
    return state.setIn(['players', state.get('currentPlayer'), "cards"], cards)
}

function playCard(state, action) {
    if (state.get('playedCard')) {
        console.error('Already played a card')
        return state
    }
    let cards = getCurrentPlayerCards(state)
    let cardIndex = cards.findIndex(card => card.id = action.id)
    if (cardIndex == -1) {
        console.error("Cannot play non-existent card with presumed id " + action.id)
        return state
    }
    let card = cards.get(cardIndex)
    state = setCurrentPlayerCards(state, cards.remove(cardIndex))
    state = state.set('playedCard', card)
    return applyCard(state, cards.get(cardIndex), action.x, action.y)
}

function drawCard(state, action) {
    let drawnCards = state.get('drawnCards') || 0
    drawnCards++
    let deck = state.get('deck')
    let card = deck.get(0)
    state = state.set('deck', deck.remove(0))
    let cards = getCurrentPlayerCards(state)
    state = setCurrentPlayerCards(state, cards.push(card))
    return state.set('drawnCards', drawnCards)
}

function addGod(state, action) {
    if (state.get('players') && state.get('players').size > 0) {
        console.error("Gods should be added to the game before players")
        return state
    }
    return state.set('gods', state.get('gods').push(action.god))
}

function addPlayer(state, action) {
    if (state.get('gods').size < 2) {
        console.error("At least two gods should be added to the game before players")
        return state
    }
    let players = state.get('players')
    let playerGod = state.get('gods').get(players.size % state.get('gods').size)
    return state.set('players', players.push(fromJS({
        name: action.name,
        god: playerGod,
        cards: []
    })))
}

function createCritter(state, action) {
    let critters = state.get('critters').push(fromJS({
        x: action.x,
        y: action.y,
        id: idCounter++
    }))
    return state.set('critters', critters)
}

function tileClicked(state, action) {
    if (!state.get('selectedCritter')) {
        return state
    }
    let critters = state.get('critters')
    let index = critters.findIndex(critter => critter.get('id') === state.get('selectedCritter'))
    if (index == -1) {
        console.warn("The id of the selected critter does not match an actual critter")
        return state
    }
    let critter = critters.get(index)
    critter = critter.set('x', action.target.x).set('y', action.target.y)
    return state.setIn(["critters", index], critter)
}