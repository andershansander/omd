import phases from '../../phases.js'

export default function (state) {
    switch (state.get('phase')) {
    case phases.RECIEVE_CARDS:
        return state.set('phase', phases.DRAW_CARDS)
    case phases.DRAW_CARDS:
        return endDrawPhase(state)
    case phases.PLAY_CARD:
        return endPlayCardPhase(state)
    default:
        console.error("Bad phase " + state.get('phase'))
        return false
    }
}

function endDrawPhase(state) {
    return state.set('drawnCards', null).set('phase', phases.PLAY_CARD)
}

function endPlayCardPhase(state) {
    return state.set('playedCard', null).set('phase', phases.ADJUST_PIETY)
}