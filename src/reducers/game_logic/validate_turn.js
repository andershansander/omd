import phases from '../../phases.js'

export default function (state) {
    switch (state.get('phase')) {
    case phases.RECIEVE_CARDS:
        return true
    case phases.DRAW_CARDS:
        return mayEndDrawPhase(state)
    case phases.PLAY_CARD:
        return mayEndPlayCardPhase(state)
    default:
        console.error("Bad phase " + state.get('phase'))
        return false
    }
    
}

function mayEndDrawPhase(state) {
    return (state.get('deck') && state.get('deck').size == 0) || (state.get('drawnCards') && state.get('drawnCards') == 2)
}


function mayEndPlayCardPhase(state) {
    return !!state.get('playedCard')
}