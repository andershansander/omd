import gameReducer from './game_reducer.js'
import {fromJS} from 'immutable'
import Phases from '../phases.js'

const defaultState = fromJS({
    game: {
        critters: [],
        players: [],
        gods: [],
        currentPlayer: 0,
        phase: Phases.RECIEVE_CARDS
    }
})

export default function (state = defaultState, action) {
    if  (action.domain == 'GAME') {
        return state.set('game', gameReducer(state.get('game'), action))
    }

    switch (action.type) {
    case '@@redux/INIT':
        //ignore
        return state
    default:
        console.warn("Unhandled action", action)
        return state
    }
}
