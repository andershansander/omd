var THREE = require('three')
import {Map} from 'immutable'
import meshFactory from './mesh_factory.js'
import inputHandler from './input_handler.js'

const height = 600
const width = 600

var scene = new THREE.Scene()
var renderer = new THREE.WebGLRenderer( { antialias: true } )
renderer.setSize( width, height )
document.body.appendChild( renderer.domElement )

var camera = new THREE.PerspectiveCamera( 70, width / height, 0.01, 50 )
camera.position.z = 3
camera.position.x = 1.5
camera.position.y = -1
camera.lookAt(new THREE.Vector3(1.5, 1, 0))

inputHandler({domElement: renderer.domElement, camera, scene, height, width})

var critters = {}
var floor = floor({width: 4, height: 4})
floor.squares.forEach(square => {scene.add(square)})

var oldCritterList

function critterHighlighting(scene, state) {
    Object.keys(critters).forEach(key => {
        if (critters[key].critterId === state.get('selectedCritter')) {
            critters[key].material.color.setHex(0x7777AA)
        } else if (critters[key].critterId === state.get('highlightedCritter')) {
            critters[key].material.color.setHex(0x445577)
        } else {
            critters[key].material.color.setHex(0x223355)
        }
    })
}

function floor({width, height, oddColor = 0x117711, evenColor = 0x779977}) {
    var squares = []
    for (let x = 0; x < width; x++) {
        for (let y = 0; y < height; y++) {
            let color = (x + y) % 2 === 0 ? evenColor : oddColor
            let mesh = meshFactory.floorSquare({x, y, color})
            mesh.position.x = x
            mesh.position.y = y
            squares.push(mesh)
        }    
    }
    return {
        width,
        height,
        squares: squares
    }
}

function syncronizeCritterMeshes(scene, newCritterList) {
    //add new and update positions
    newCritterList.forEach(critter => {
        let id = critter.get('id')
        if (!critters[id]) {
            critters[id] = meshFactory.critter(id)
            critters[id].position.z = 0.5
            scene.add(critters[id])
        }
        critters[id].position.x = critter.get('x')
        critters[id].position.y = critter.get('y')
    })
    //remove obsolete
    Object.keys(critters).forEach(key => {
        if (newCritterList.findIndex(critter => critter.get('id') == key) == -1) {
            scene.remove(critters[key])
            delete critters[key]
        }
    })
}

export default function (state) {
    var newCritterList = state.get('critters')
    if (newCritterList != oldCritterList) {
        syncronizeCritterMeshes(scene, newCritterList)
        oldCritterList = newCritterList
    }
    
    critterHighlighting(scene, state)
    renderer.render(scene, camera)
}