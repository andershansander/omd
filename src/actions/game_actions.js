export function endTurn() {
    return {
        domain: 'GAME',
        type: "END_TURN",
    }
}

export function drawCard() {
    return {
        domain: 'GAME',
        type: "DRAW_CARD",
    }
}

export function playCard({id, x, y}) {
    return {
        domain: 'GAME',
        type: "PLAY_CARD",
        id,
        x,
        y
    }
}

export function addGod({god}) {
    return {
        domain: 'GAME',
        type: "ADD_GOD",
        god
    }
}
export function addPlayer({name}) {
    return {
        domain: 'GAME',
        type: "ADD_PLAYER",
        name
    }
}

export function addCritter({x, y}) {
    return {
        domain: 'GAME',
        type: "CREATE_CRITTER",
        x,
        y
    }
}

export function tileClicked({target}) {
    return {
        domain: 'GAME',
        type: "TILE_CLICKED",
        target
    }
}

export function critterHovered({id}) {
    return {
        domain: 'GAME',
        type: "CRITTER_HOVERED",
        id
    }
}

export function critterClicked({id}) {
    return {
        domain: 'GAME',
        type: "CRITTER_CLICKED",
        id
    }
}

export function useDeck({deck}) {
    return {
        domain: 'GAME',
        type: "USE_DECK",
        deck
    }
}