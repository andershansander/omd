import React from 'react'
import { connect } from 'react-redux'
import {List} from 'immutable'
import Players from './Players.js'
import DrawCards from './DrawCards.js'
import Cards from './Cards.js'
import EndTurn from './EndTurn.js'
import phases from '../phases.js'

const phaseTranslations = [
    "Recieve cards",
    "Draw cards",
    "Play card",
    
]
const Game = ({ players, phase, currentPlayer }) => {
    if (players.size == 0) {
        return <span>No players yet</span>
    }
    return <div className="game-info">
        Current player: {players.get(currentPlayer).get('name')}<br />
        Phase: {phaseTranslations[phase - 1]}
        <Players />
        <Cards />
        {phase == phases.DRAW_CARDS ? <DrawCards /> : null}
        <EndTurn />
    </div>
}

const mapStateToProps = state => {
    return {
        phase: state.get('game').get('phase'),
        currentPlayer: state.get('game').get('currentPlayer'),
        players: state.get('game').get('players') || List()
    }
  }

export default connect(mapStateToProps)(Game)