import React from 'react'
import { connect } from 'react-redux'
import {List} from 'immutable'
import phases from '../phases.js'

import {playCard as playCardAction} from '../actions/game_actions.js'
import store from '../store.js'

function playCard(id) {
    store.dispatch(playCardAction({id, x: 1, y: 1}))
}
function cardListItem(card, playable) {
    return playable ? <li className="playable" onClick={playCard.bind(null, card.get('id'))} key={card.get('id')}>{card.get('name')}</li> : <li key={card.get('id')}>{card.get('name')}</li>
}
const Cards = ({ cards, phase, hasPlayedCard}) => {
    const canPlayCard = !hasPlayedCard && phase == phases.PLAY_CARD
    return <ul>
        {cards.map(card => cardListItem(card, canPlayCard))}
        </ul>
}

const mapStateToProps = state => {
    let currentPlayer = state.get('game').get('currentPlayer')
    return {
        hasPlayedCard: state.getIn(['game', 'playedCard']),
        phase: state.get('game').get('phase'),
        cards: state.get('game').get('players').get(currentPlayer).get('cards') || List()
    }
  }

export default connect(mapStateToProps)(Cards)