import React from 'react'
import { connect } from 'react-redux'
import {List} from 'immutable'

const Players = ({ players }) => {
    if (players.size == 0) {
        return <span />
    }
    return <ul>
        {players.map(player => <li key={player.get('name')}>{player.get('name') + " - " + player.get('god')}</li>)}
        </ul>
}

const mapStateToProps = state => {
    return {
      players: state.get('game').get('players') || List()
    }
  }

export default connect(mapStateToProps)(Players)