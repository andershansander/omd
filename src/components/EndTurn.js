import React from 'react'
import { connect } from 'react-redux'
import {endTurn} from '../actions/game_actions.js'
import store from '../store.js'

const EndTurn = ({canEndTurn}) => {
    return <button disabled={!canEndTurn} onClick={() => {store.dispatch(endTurn())}}>End turn</button>
}

const mapStateToProps = state => {
    return {
        canEndTurn: state.get('game').get('canEndTurn')
    }
  }

export default connect(mapStateToProps)(EndTurn)