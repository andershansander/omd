import React from 'react'
import { connect } from 'react-redux'
import {drawCard} from '../actions/game_actions.js'
import store from '../store.js'

const DrawCards = () => {
    return <div>
        <button onClick={() => {store.dispatch(drawCard())}}>Draw card</button>
    </div>
}

const mapStateToProps = state => {
    return {
      
    }
  }

export default connect(mapStateToProps)(DrawCards)