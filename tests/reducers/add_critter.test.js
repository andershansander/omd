import rootReducer from '../../src/reducers/root_reducer.js'
import {addCritter} from '../../src/actions/game_actions.js'

describe('add critter', () => {
  it('should add one critter to list', () => {
    const state = rootReducer(undefined, addCritter({x: 1, y: 2}))
    expect(state.get('game').get('critters').size).toEqual(1)
    expect(state.get('game').get('critters').get(0).get('x')).toEqual(1)
    expect(state.get('game').get('critters').get(0).get('y')).toEqual(2)
  })
})