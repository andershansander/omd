import rootReducer from '../../src/reducers/root_reducer.js'

describe('default state', () => {
  it('should contain an empty critters list', () => {
    const state = rootReducer(undefined, {type: '@@redux/INIT'})
    expect(state.get('game').get('critters').size).toEqual(0)
  })
})