import rootReducer from '../../src/reducers/root_reducer.js'
import {addPlayer, addGod} from '../../src/actions/game_actions.js'

describe('adding players and gods', () => {
    it('needs gods before players can be added', () => {
        const state = rootReducer(undefined, addPlayer({name: 'Pogge'}))
        expect(state.get('game').get('players').size).toEqual(0)
    })
    it('needs two gods before players can be added', () => {
        let state = rootReducer(undefined, addGod({god: 'Blood'}))
        state = rootReducer(state, addPlayer({name: 'Pogge'}))
        expect(state.get('game').get('gods').size).toEqual(1)
        expect(state.get('game').get('players').size).toEqual(0)
    })
    it('players should be distributed evenly across gods', () => {
        let state = rootReducer(undefined, addGod({god: 'Blood'}))
        state = rootReducer(state, addGod({god: 'Rabbit'}))
        state = rootReducer(state, addPlayer({name: 'Pogge'}))
        state = rootReducer(state, addPlayer({name: 'Barsko'}))
        state = rootReducer(state, addPlayer({name: 'Tirden'}))
        state = rootReducer(state, addPlayer({name: 'Gridolf'}))
        expect(state.get('game').get('gods').size).toEqual(2)
        expect(state.get('game').get('players').size).toEqual(4)
        expect(state.get('game').get('players').get(0).get('god')).toEqual("Blood")
        expect(state.get('game').get('players').get(1).get('god')).toEqual("Rabbit")
        expect(state.get('game').get('players').get(2).get('god')).toEqual("Blood")
        expect(state.get('game').get('players').get(3).get('god')).toEqual("Rabbit")
    })
})